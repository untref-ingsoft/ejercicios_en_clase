@wip
Feature: Recomendaciones De Titulos

    Description: Comprobamos el feature de recomendaciones de titulos para usuarios

    Background:
        Given la pelicula "El crimen" de categoria "terror"
        Given la pelicula "Terror profundo" de categoria "terror"
        Given la pelicula "El circo" de categoria "risa"
        Given la pelicula novedad "Ratched" de categoria "terror"
        Given la pelicula novedad "El abuelo" de categoria "amor"
    
    Scenario: Un usuario que no tiene nada en su lista de recomendaciones
        Given El usuario no tiene nada 
        When pide recomendaciones
        Then solo se le muestran "Ratched" y "El abuelo"
    
    Scenario: Un usuario puso me gusta a pelis de una categoria
        Given El usuario puso me gusta a "Rarched"
        And El usuario puso me gusta a "El crimen"
        When pide recomendaciones
        Then se le muestra "Terror profundo"

    Scenario: Un usuario puso no me gusta a una novedad entonces no se la muestra como recomendacion
        Given El usuario puso no me gusta a "Ratched"
        When pide recomendaciones
        Then no se le muestra "Ratched"
        But se le muestra "El abuelo"
    