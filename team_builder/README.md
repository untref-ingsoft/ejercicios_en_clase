Armador de equipos
==================

En la consultora X han establecido un conjunto de reglas para definir los equipos de proyectos en base a un conjunto de parámetros del proyecto y una serie de reglas.
Se pide entonces desarrollar una aplicación de línea de comando que tome los parámetos de proyecto y genere un equipo de proyecto minimizando el costo total del mismo.


Datos de proyecto (entrada de la aplicación):

* Cantidad mensual de horas de desarrollo que requerirá el proyecto
* Cantidad de meses que durará el proyecto
* Tecnología con la que se desarrollará el proyecto

Reglas:

* Todo proyecto tiene al menos un desarrollador senior (ds)
* Los proyectos de más de 1000 horas de desarrollo requieren de un Tester/Analista (ta) por cada 3 desarrolladores (mínimo 1)
* Los proyectos de más de 3 meses de duración requiren de un PM
* Los proyectos en tecnología ruby no puede tener más de 1 desarrollador junior
* Un desarrollador Senior (ds) trabaja 40 horas por semana y tiene un costo de $ 2000 /hora
* Desarrollador Junior (dj) trabaja 30 horas por semana y tiene un costo de $ 1000 /hora
* Analista / Tester (at) trabaja 40 horas por semana y tiene un costo de 800 /hora
* Project Manager (pm) trabaja 10 horas por semana y tiene un curso de 3000 /hora


El desarrollo de la aplicación debe hacerse siguiendo el ciclo BDD + TDD.

El archivo acceptance_test.sh contiene un conjunto de pruebas funcionales desde la perspectiva del usuario y debe usarse para guiar el ciclo macro propuesto por BDD.

Las pruebas de índole unitarias correspondiente al ciclo de TDD deben ser diseñadas y construidas como parte de la consigna. Dichas pruebas dependerán del diseño de objetos que cada uno realice.

Tener presente que Gitlab no ejecutará las pruebas de aceptación con lo cual pueden (y deben) hacer commit+push aún cuando tengan pruebas de aceptación que no estén funcionando.

Importante: no basta con que la aplicación pase las pruebas de aceptación, eso es solo condición necesaria pero no suficiente. Es necesario que la aplicación tenga la lógica completa de cálculo.
