class Parser
  def initialize(input)
    @input = input
  end

  def tipo_vehiculo
    @input.split('/')[0].to_s
  end

  
  def kilometraje
    @input.split('/')[2].to_i
  end

  def cilindrada
    @input.split('/')[1].to_i
  end

  def combustible
    @input.split('/')[3].to_s
  end

  def anio_fabricacion
    @input.split('/')[4].to_i
  end
end
