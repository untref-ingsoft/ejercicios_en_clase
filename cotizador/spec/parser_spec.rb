require 'spec_helper'

require_relative '../model/parser'

describe 'Parser' do
  it 'deberia leer tipo_vehiculo' do
    parser = Parser.new('auto/1600/300/gas/2020')
    expect(parser.tipo_vehiculo).to eq 'auto'
  end

  it 'deberia leer cilindrada' do
    parser = Parser.new('auto/1600/300/gas/2020')
    expect(parser.cilindrada).to eq 1600
  end

  it 'deberia leer suv' do
    parser = Parser.new('suv/1600/300/gas/2020')
    expect(parser.tipo_vehiculo).to eq 'suv'
  end

  it 'deberia leer 300km' do
    parser = Parser.new('suv/1600/300/gas/2020')
    expect(parser.kilometraje).to eq 300
  end

  it 'deberia leer gas para combustible' do
    parser = Parser.new('suv/1600/300/gas/2020')
    expect(parser.combustible).to eq 'gas'
  end

  it 'deberia leer 2020 para anio de fabricacion' do
    parser = Parser.new('suv/1600/300/gas/2020')
    expect(parser.anio_fabricacion).to eq 2020
  end
end
