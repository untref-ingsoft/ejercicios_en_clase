require 'spec_helper'
require_relative '../model/chopper'

describe 'App' do
  xit 'calcula cotizacion para auto con gas 0 km' do
    resultado = `ruby app.rb auto/1600/300/gas/2020`
    expect(resultado.strip).to eq 'vi:$1.600.000 vr:$1.600.000 vu:1.600'
  end

  xit 'calcula cotizacion para suv con gas 0 km' do
    resultado = `ruby app.rb suv/1600/300/gas/2020`
    expect(resultado.strip).to eq 'vi:$1.920.000 vr:$1.920.000 vu:1.600'
  end
end
