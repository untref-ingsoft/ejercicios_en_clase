require 'spec_helper'
require_relative '../model/cotizador'

describe 'Cotizador' do
    xit 'deberia devolver el valor impositivo' do
        cotizador = Cotizador.new
        cotizacion = cotizador.cotizar('auto', 1600, 300, 'gas', 2020)
        expect(cotizacion.valor_impositivo).to eq 1600000
    end
end
